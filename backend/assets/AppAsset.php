<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /** Not for Template Adminlte 3 **/
        'css/site.css',
    ];
    public $js = [
        'js/main.js', //For Dinamic Form Ajax
    ];
    public $depends = [
        /** Not for Template Adminlte 3 **/
        /*'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',*/
    ];
}
