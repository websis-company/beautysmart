<?php

namespace backend\controllers;

use Yii;
use backend\models\Course;
use backend\models\CourseSearch;
use backend\models\Category;
use backend\models\Instructor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class'=> AccessControl::className(),
                'only' => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow' =>true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function getCategoryTree(){
        $models       = Category::find()->where(['categoryparent_id' => null,'estatus' => 'active'])->all();
        $categoryTree = [];
        foreach ($models as $model) {
            $rel_categories = $model->categories;
            $par_category   = $model->category_id;
            $categoryTree[$par_category] = ["id"=>$par_category,"name"=>$model->name];
            foreach ($rel_categories as $category) {
                $categoryTree[$par_category]["children"][] = ["id"=>$category->category_id,"name"=>$category->name];
            }//end foreach
        }//end foreach

        //
        return $categoryTree;
    }

    public function getOptionsCategoryTree($categoryTree = []){
        $categoryOptions = [];
        $d = 0;
        foreach ($categoryTree as $item) {
            $categoryOptions[$d]["id"]   = $item["id"];
            $categoryOptions[$d]["name"] = $item["name"];

            if(isset($item["children"])){
                foreach ($item["children"] as $itemChildren) {
                    $d++;
                    $categoryOptions[$d]["id"]   = $itemChildren["id"];
                    $categoryOptions[$d]["name"] = "&nbsp;&nbsp;&nbsp; - ".$itemChildren["name"];
                }
            }//end if
            $d++;
        }//end foreach*/

        //
        return $categoryOptions;
    }

    public function getAllInstructors(){
        $instructorOptions = [];
        $instructors       = Instructor::find()->where(['estatus' => 'active'])->all();
        $i = 0;
        foreach ($instructors as $instructor) {
            $instructorOptions[$i]["id"]   = $instructor->instructor_id;
            $instructorOptions[$i]["name"] = $instructor->firstname." ".$instructor->lastname;
            $i++;
        }//end foreach
        //
        return $instructorOptions;
    }//end function

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model              = new Course();
        $categoryTree       = $this->getCategoryTree();
        $categoryOptions    = $this->getOptionsCategoryTree($categoryTree);
        $instructorsOptions = $this->getAllInstructors();

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->created_at)){
                $model->created_at = date('Y-m-d',strtotime($model->created_at));
            }else{
                $model->created_at = null;
            }//end if

            $model->save();

            Yii::$app->session->setFlash('success', "Se registro correctamente el curso :  <strong>".$model->category->name." : ".$model->title."</strong>");
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model'              => $model,
            'categoryOptions'    => $categoryOptions,
            'instructorsOptions' => $instructorsOptions
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model              = $this->findModel($id);
        $categoryTree       = $this->getCategoryTree();
        $categoryOptions    = $this->getOptionsCategoryTree($categoryTree);
        $instructorsOptions = $this->getAllInstructors();

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->created_at)){
                $model->created_at = date('Y-m-d',strtotime($model->created_at));
            }else{
                $model->created_at = null;
            }//end if

            $model->save();
            
            Yii::$app->session->setFlash('success', "Se actualizo correctamente el curso :  <strong>".$model->category->name." : ".$model->title."</strong>");
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model'              => $model,
            'categoryOptions'    => $categoryOptions,
            'instructorsOptions' => $instructorsOptions
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
