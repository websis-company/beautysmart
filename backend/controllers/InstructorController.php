<?php

namespace backend\controllers;

use Yii;
use backend\models\Instructor;
use backend\models\InstructorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * InstructorController implements the CRUD actions for Instructor model.
 */
class InstructorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access'=>[
                'class'=> AccessControl::className(),
                'only' => ['index','create','update','delete'],
                'rules' => [
                    [
                        'allow' =>true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Instructor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InstructorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instructor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Instructor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Instructor();

        if ($model->load(Yii::$app->request->post())){
            $model->imageProfile = UploadedFile::getInstance($model,'imageProfile');
            if(!empty($model->imageProfile)){
                $imageName          = $model->imageProfile->name;
                $model->imageProfile->saveAs('uploads/instructores/'.$imageName);
                $model->perfilimage = 'uploads/instructores/'.$imageName;
            }else{
                $model->perfilimage = null;
            }//end if

            $model->image_1 = UploadedFile::getInstance($model,'image_1');
            if(!empty($model->image_1)){
                $image_1_name  = $model->image_1->name;
                $model->image_1->saveAs('uploads/instructores/'.$image_1_name);
                $model->image1 = 'uploads/instructores/'.$image_1_name;
            }else{
                $model->image1 = null;
            }//end if

            $model->image_2 = UploadedFile::getInstance($model,'image_2');
            if(!empty($model->image_2)){
                $image_2_name  = $model->image_2->name;
                $model->image_2->saveAs('uploads/instructores/'.$image_2_name);
                $model->image2 = 'uploads/instructores/'.$image_2_name;
            }else{
                $model->image2 = null;
            }//end if

            $model->image_3 = UploadedFile::getInstance($model,'image_3');
            if(!empty($model->image_3)){
                $image_3_name  = $model->image_3->name;
                $model->image_3->saveAs('uploads/instructores/'.$image_3_name);
                $model->image3 = 'uploads/instructores/'.$image_3_name;
            }else{
                $model->image3 = null;
            }//end if

            $model->image_4 = UploadedFile::getInstance($model,'image_4');
            if(!empty($model->image_4)){
                $image_4_name  = $model->image_4->name;
                $model->image_4->saveAs('uploads/instructores/'.$image_4_name);
                $model->image4 = 'uploads/instructores/'.$image_4_name;
            }else{
                $model->image4 = null;
            }//end if

            $model->save(false);

            Yii::$app->session->setFlash('success', "Se registro correctamente la información de :  <strong>".$model->firstname." ".$model->lastname."</strong>");
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Instructor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageProfile = UploadedFile::getInstance($model,'imageProfile');
            if(!empty($model->imageProfile)){
                $imageName          = $model->imageProfile->name;
                $model->imageProfile->saveAs('uploads/instructores/'.$imageName);
                $model->perfilimage = 'uploads/instructores/'.$imageName;  
            }//end if

            $model->image_1 = UploadedFile::getInstance($model,'image_1');
            if(!empty($model->image_1)){
                $image_1_name  = $model->image_1->name;
                $model->image_1->saveAs('uploads/instructores/'.$image_1_name);
                $model->image1 = 'uploads/instructores/'.$image_1_name;
            }//end if

            $model->image_2 = UploadedFile::getInstance($model,'image_2');
            if(!empty($model->image_2)){
                $image_2_name  = $model->image_2->name;
                $model->image_2->saveAs('uploads/instructores/'.$image_2_name);
                $model->image2 = 'uploads/instructores/'.$image_2_name;
            }//end if

            $model->image_3 = UploadedFile::getInstance($model,'image_3');
            if(!empty($model->image_3)){
                $image_3_name  = $model->image_3->name;
                $model->image_3->saveAs('uploads/instructores/'.$image_3_name);
                $model->image3 = 'uploads/instructores/'.$image_3_name;
            }//end if

            $model->image_4 = UploadedFile::getInstance($model,'image_4');
            if(!empty($model->image_4)){
                $image_4_name  = $model->image_4->name;
                $model->image_4->saveAs('uploads/instructores/'.$image_4_name);
                $model->image4 = 'uploads/instructores/'.$image_4_name;
            }//end if


            $model->save(false);

            Yii::$app->session->setFlash('success', "Se actualizo correctamente la información de :  <strong>".$model->firstname." ".$model->lastname."</strong>");
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Instructor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instructor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Instructor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Instructor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
