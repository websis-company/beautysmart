<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%instructor}}".
 *
 * @property int $instructor_id
 * @property string $firstname
 * @property string $lastname
 * @property string $trajectory
 * @property string|null $perfilimage
 * @property string|null $image1
 * @property string|null $image2
 * @property string|null $image3
 * @property string|null $image4
 * @property string|null $fblink
 * @property string|null $iglink
 * @property string|null $wplink
 * @property string|null $twlink
 * @property string|null $estatus
 */
class Instructor extends \yii\db\ActiveRecord
{
    public $imageProfile;
    public $image_1;
    public $image_2;
    public $image_3;
    public $image_4;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%instructor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'trajectory','estatus'], 'required'],
            [['trajectory', 'estatus'], 'string'],
            [['perfilimage'],'safe'],
            [['imageProfile'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 190,'maxWidth'=>500,'minHeight'=>190,'maxHeight'=>500,'maxSize'=>1024 * 1024 * 2],
            [['image_1'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 315,'maxWidth'=>315,'minHeight'=>471,'maxHeight'=>471,'maxSize'=>1024 * 1024 * 2],
            [['image_2'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 315,'maxWidth'=>315,'minHeight'=>209,'maxHeight'=>209,'maxSize'=>1024 * 1024 * 2],
            [['image_3'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 315,'maxWidth'=>315,'minHeight'=>471,'maxHeight'=>471,'maxSize'=>1024 * 1024 * 2],
            [['image_4'],'image','extensions'=>'jpeg,jpg,png,gif','minWidth' => 315,'maxWidth'=>315,'minHeight'=>209,'maxHeight'=>209,'maxSize'=>1024 * 1024 * 2],
            [['firstname', 'lastname', 'image1', 'image2', 'image3', 'image4', 'fblink', 'iglink', 'wplink', 'twlink'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'instructor_id' => 'Instructor ID',
            'firstname'     => 'Nombre(s)',
            'lastname'      => 'Apellido(s)',
            'trajectory'    => 'Trayectoría',
            'perfilimage'   => 'Foto',
            'fileMainImage' => 'Foto',
            'image1'        => 'Image1',
            'image2'        => 'Image2',
            'image3'        => 'Image3',
            'image4'        => 'Image4',
            'fblink'        => 'Facebook',
            'iglink'        => 'Instagram',
            'wplink'        => 'WhatsApp',
            'twlink'        => 'Twitter',
            'estatus'       => 'Estatus',
        ];
    }
}
