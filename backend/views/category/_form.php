<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\bootstrap4\ActiveForm */
?>


    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'categoryForm']]); ?>

<div class="category-form card-body">
    <?php 
    $principalCategories = [];
    foreach($parentsCategory  as $principal){
        $principalCategories[$principal->category_id] = $principal->name;
    }

    ?>
    <?php 
        // Normal select with ActiveForm & model [Select2]
        /*echo $form->field($model,'categoryparent_id')->label('<span class="text-dark">Categoría Padre</span>')->widget(Select2::classname(), [
            'data'     => $principalCategories,
            'language' => 'es',
            //'theme'    =>Select2::THEME_BOOTSTRAP,
            'options'  => [
                'value'       => $model->categoryparent_id,
                'placeholder' => 'Seleccione una categoría'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]);*/
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="row">
        <?= $form->field($model, 'sliderImages',['options'=>['class'=>'col-12 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen Carrusel (Home): </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 352px X 528px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview_sliderimages" class="col-12 mt-3" align="center"></div>
    </div>

    <?= $form->field($model, 'estatus')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Seleccione una opción']) ?>

</div>
<div class=" card-footer" align="right">
	<?=  Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-danger','id'=>'btnCloseForm','onClick'=>'closeForm("categoryForm")']) ?>
    <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    document.getElementById("category-sliderimages").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview_sliderimages').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview_sliderimages'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }
JS;
$this->registerJs($script);
?>

