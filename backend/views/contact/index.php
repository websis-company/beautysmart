<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="loading text-center"></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-6 offset-lg-3" style="display: none;"></div>
</div>

<div class="contact-index">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <div class="col-6 float-right pb-3">
                            <?php echo Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Contacto', ['value'=>Url::to('create'),'class' => 'btn bg-gradient-red float-right','id'=>'btnAddForm']);
                            ?>
                        </div>
                    </div>
                    <div class="card-body pad table-responsive">
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                //'contact_id',
                                'name',
                                'email:email',
                                'subject',
                                'body:ntext',
                                //'created_at',

                                [
                                    'class'         => 'yii\grid\ActionColumn',
                                    'header'        => 'Actions',
                                    'headerOptions' => ['style'=>'text-align:center'],
                                    'template'      => '{view} {update} {delete}',
                                    'buttons'       => [
                                        'view'=>function($url,$model){
                                            return Html::button('<i class="fas fa-eye"></i>',['value'=>Url::to(['view','id'=>$model->contact_id]), 'class' => 'btn bg-teal btn-sm btnViewForm', 'title'=>'Consultar']);
                                        },
                                        'update'=>function ($url, $model) {
                                            return Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id'=>$model->contact_id]), 'class' => 'btn bg-teal btn-sm btnUpdateForm','title'=>'Editar']);
                                        },
                                        'delete'=>function ($url, $model) {
                                            return Html::a('<i class="fas fa-trash-alt"></i>', $url = Url::to(['delete','id'=>$model->contact_id]), ['class' => 'btn bg-danger btn-sm','title'=>'Eliminar','data-pajax'=>0, 'data-confirm'=>'¿Está seguro de eliminar este elemento?','data-method'=>'post']);
                                        },
                                    ]
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
