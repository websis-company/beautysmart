<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Course */
/* @var $form yii\bootstrap4\ActiveForm */
?>


<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'courseForm']]); ?>
<div class="course-form card-body ">

    <?php 
    //options generator for select2 widget
    $options = [];
    foreach ($categoryOptions as $option) {
        $options[$option["id"]] = html_entity_decode($option["name"]);
    }//end foreach

    // Normal select with ActiveForm & model [Select2]
    echo $form->field($model,'category_id',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->label('<span class="text-dark">Categoría</span>')->widget(Select2::classname(),[
        'data'     => $options,
        'language' => 'es',
        //'theme'    => Select2::THEME_BOOTSTRAP,
        'options' => [
            'value' => $model->category_id,
            'placeholder' => 'Selecione una categoría'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'title',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->dropDownList([ 'basico' => 'Básico', 'avanzado' => 'Avanzado', ], ['prompt' => 'Seleccione una opción'])?>
    <!-- <?= $form->field($model, 'author',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?> -->

    <?php 
    $instructors = [];
    foreach($instructorsOptions  as $instructor){
        $instructors[$instructor["id"]] = $instructor["name"];
    }//end foreach
    // Normal select with ActiveForm & model [Select2]
    echo $form->field($model,'instructor_id',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->label('<span class="text-dark">Instructor</span>')->widget(Select2::classname(),[
        'data'     => $instructors,
        'language' => 'es',
        //'theme'    => Select2::THEME_BOOTSTRAP,
        'options' => [
            'value' => $model->instructor_id,
            'placeholder' => 'Selecione un instructor'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'code',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'introduction',['options'=>['class'=>'col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'videointro',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textInput()->label('<div>Video Presentación (Url): </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Ej. https://vimeo.com/288344114 </small></div>',['class'=>'col-12']) ?>
    
    <?= $form->field($model, 'topics',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'materials',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'description',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'requirements',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>

    <?php /*echo  $form->field($model, 'created_at',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->widget(DatePicker::classname(),[
            'name' => 'created_at', 
            'value' => date('d-M-Y'),
            'options' => ['placeholder' => 'Seleccione la fecha de inicio'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-M-yyyy',
                'todayHighlight' => true
            ]
        ])*/
    ?>
    
    <?php /*echo  $form->field($model, 'hours',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['type'=>'number'])*/ ?>
    <?= $form->field($model, 'price',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['type' => 'number','maxlength' => true]) ?>
    <?= $form->field($model, 'estatus',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Seleccione una opción']) ?>

    
    <!-- <?//= $form->field($model, 'articles')->textInput() ?> -->
    <!-- <?//= $form->field($model, 'discount')->textInput() ?> -->
    <!-- <?= $form->field($model, 'mainimage')->textInput() ?>
    <?= $form->field($model, 'secondaryimage')->textInput() ?>
    <?= $form->field($model, 'thirdimage')->textInput() ?>
    <?= $form->field($model, 'bannerimage')->textInput() ?> -->

</div>
<div class=" card-footer" align="right">
	<?=  Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-danger','id'=>'btnCloseForm','onClick'=>'closeForm("courseForm")']) ?>
    <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

