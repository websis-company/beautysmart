<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use backend\models\Category;
use backend\models\Instructor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Beauty Smart | Cursos';
$this->params['breadcrumbs'][] = $this->title;
echo newerton\fancybox3\FancyBox::widget([
    'target' => '.data-fancybox',
]);
?>

<div class="container-fluid">
    <div class="loading text-center"></div>
    <div id="divEditForm" class="col-sm-12 col-md-12 col-lg-10 offset-lg-1" style="display: none;"></div>
</div>

<div class="app\models\Course-index">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="col-6 float-right pb-3">
                        <?= Html::button('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Agregar Course', ['value' => Url::to('create'), 'class' => 'btn bg-gradient-danger float-right','id'=>'btnAddForm']) ?>
                    </div>
                </div>
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="row-fluid mt-2" align="center">
                        <div class="col-sm-12">
                            <div class="alert bg-teal alert-dismissable">
                               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                               <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('success') ?>
                           </div>
                        </div>
                    </div>
               <?php endif; ?>

               <?php if (Yii::$app->session->hasFlash('danger')): ?>
                    <div class="row-fluid mt-2" align="center">
                        <div class="col-sm-12">
                            <div class="alert bg-danger alert-dismissable">
                               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                               <i class="icon fa fa-check"></i> <?= Yii::$app->session->getFlash('danger') ?>
                           </div>
                        </div>
                    </div>
               <?php endif; ?>
               
                <div class="card-body pad table-responsive">


                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?php 
                    $filterIns = Instructor::find()->all();
                    $filterInstructor = [];
                    foreach ($filterIns as $instructor) {
                        $filterInstructor[$instructor->instructor_id] = $instructor->firstname." ".$instructor->lastname;
                    }//end foreach

                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'course_id',
                            [
                                'attribute' => 'category_id',
                                'format'    =>'html',
                                'value' => function($model){
                                    if(is_null($model->category->name)){
                                        return "-";
                                    }else{
                                        return $model->category->name;
                                    }
                                },
                                'filter' => Html::activeDropDownList(
                                    $searchModel,
                                    'category_id',
                                    ArrayHelper::map(Category::find()->all(),'category_id','name'),
                                    ['class' => 'form-control','prompt'=>'Todos']
                                ),
                            ],
                            [
                                'attribute' => 'title',
                                'value' => function($model){
                                    if($model->title == 'basico'){
                                        return "Básico";
                                    }else{
                                        return "Avanzado";
                                    }
                                },
                                'filter' =>  Html::activeDropDownList($searchModel,'title',['basico'=>'Básico','avanzado'=>'Avanzado'],['class' => 'form-control','prompt'=>'Todos'])
                            ],
                            'code',
                            //'category_id',
                            //'title',
                            'introduction',
                            [
                                'label' => 'Presentación',
                                'attribute' => 'videointro',
                                'format' => 'html',
                                'contentOptions' => ['style'=>'text-align: center'],
                                'value' => function($model){
                                    return Html::a('Ver Video',$url=$model->videointro,['title'=>'Ver Video','class' => 'data-fancybox btn btn-info']);
                                }, 
                            ],
                            [
                                'attribute' => 'instructor_id',
                                'value' => function($model){
                                    if(is_null($model->instructor->firstname)){
                                        return "-";
                                    }else{
                                        return $model->instructor->firstname." ".$model->instructor->lastname;
                                    }
                                }, 
                                'filter' => Html::activeDropDownList(
                                    $searchModel,
                                    'instructor_id',
                                    $filterInstructor,
                                    ['class' => 'form-control','prompt'=>'Todos']
                                ),
                            ],

                            //'instructor_id',
                            //'description:ntext',
                            //'created_at',
                            //'requirements:ntext',
                            //'hours',
                            //'articles',
                            //'price',
                            //'discount',
                            [
                                'attribute'   => 'estatus',
                                'format'      => 'html',
                                'contentOptions' => ['style'=>'text-align: center'],
                                'value'       => function($model){
                                    if($model->estatus == "active"){
                                        return '<div class="right badge badge-success">Activo</div>';
                                    }else{
                                        return '<div class="right badge badge-danger">Inactivo</div>';
                                    }
                                },
                                'filter' => Html::activeDropDownList($searchModel,'estatus',['active'=>'Activo','inactive'=>'Inactivo'],['class' => 'form-control','prompt'=>'Todos'])
                            ],

                            [
                                'class' => 'hail812\adminlte3\yii\grid\ActionColumn',
                                'header'        => 'Actions',
                                'headerOptions' => ['style'=>'text-align:center'],
                                'template'      => '{view} {update} {delete}',
                                'buttons'       => [
                                    'view'=>function($url,$model){
                                        return Html::button('<i class="fas fa-eye"></i>',['value'=>Url::to(['view', 'id' => $model->course_id]), 'class' => 'btn bg-teal btn-sm btnViewForm', 'title'=>'Consultar']);
                                    },
                                    'update'=>function ($url, $model) {
                                        return Html::button('<i class="fas fa-edit"></i>',['value'=>Url::to(['update','id' => $model->course_id]), 'class' => 'btn bg-teal btn-sm btnUpdateForm','title'=>'Editar']);
                                    },
                                    'delete'=>function ($url, $model) {
                                        return Html::a('<i class="fas fa-trash-alt"></i>', $url = Url::to(['delete','id' => $model->course_id]), ['class' => 'btn bg-danger btn-sm','title'=>'Eliminar','data-pajax'=>0, 'data-confirm'=>'¿Está seguro de eliminar este elemento?','data-method'=>'post']);
                                    },
                                ]

                            ],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ]
                    ]); ?>


                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
</div>
