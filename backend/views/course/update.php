<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Course */
$tmp = $model->title == 'basico' ? 'Básico' : 'Avanzado';
$this->title = 'Actualizar Curso: '.$model->category->name." (".$tmp.")";
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-update">
    <div class="row-fluid">
        <div class="col-sm-12">
            <div class="card card-danger">
                <div class="card-header">
                    <h1 class="card-title"><strong><i class="nav-icon fas fa-edit"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($this->title) ?></strong></h1>
                    <button type="button" class="btn close text-white" onclick='closeForm("courseForm")'>×</button>
                </div>
                <?=$this->render('_form', [
                    'model'              => $model,
                    'categoryOptions'    => $categoryOptions,
                    'instructorsOptions' => $instructorsOptions
                ]) ?>
            </div>
            <!--.card-->
        </div>    
    </div>
</div>