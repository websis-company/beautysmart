<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Instructor */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'instructorForm']]); ?>
<div class="instructor-form card-body">
    <?= $form->field($model, 'firstname',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastname',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'trajectory',['options'=>['class'=>'col-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'imageProfile',['options'=>['class'=>'col-12 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Foto de perfil: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 500px X 500px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
    <div class="clearfix"></div>
    <div id="preview" class="col-12 container" style="" align="center"></div>
    <div class="clearfix"></div>

    <?= $form->field($model, 'fblink',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'iglink',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'wplink',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'twlink',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>
    
    <div class="clearfix"></div>
    <hr>
    <fieldset>
        <h2>Galería</h2>
    </fieldset>
    <div class="row">
        <?= $form->field($model, 'image_1',['options'=>['class'=>'col-sm-12 col-md-10 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen 1: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 315px X 471px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview_img1" class="col-sm-12 col-md-2 mt-3" align="center"></div>
        <?= $form->field($model, 'image_2',['options'=>['class'=>'col-sm-12 col-md-10 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen 2: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 315px X 209px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview_img2" class="col-sm-12 col-md-2 mt-3" align="center"></div>
    </div>
    <div class="row">
        <?= $form->field($model, 'image_3',['options'=>['class'=>'col-sm-12 col-md-10 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen 3: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 315px X 471px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview_img3" class="col-sm-12 col-md-2 mt-3" align="center"></div>
        <?= $form->field($model, 'image_4',['options'=>['class'=>'col-sm-12 col-md-10 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen 4: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 315px X 209px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview_img4" class="col-sm-12 col-md-2 mt-3" align="center"></div>
    </div>

    <div class="clearfix"></div>
    <?= $form->field($model, 'estatus',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->dropDownList([ 'active' => 'Activo', 'inactive' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>

</div>
<div class=" card-footer" align="right">
    <?=  Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-danger','id'=>'btnCloseForm','onClick'=>'closeForm("instructorForm")']) ?>
    <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    document.getElementById("instructor-imageprofile").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }

    document.getElementById("instructor-image_1").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview_img1').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview_img1'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }

    document.getElementById("instructor-image_2").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview_img2').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview_img2'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }

    document.getElementById("instructor-image_3").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview_img3').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview_img3'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }

    document.getElementById("instructor-image_4").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview_img4').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview_img4'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }
JS;
$this->registerJs($script);
?>