<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\bootstrap4\ActiveForm */
?>


    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'productForm']]); ?>

<div class="product-form card-body">
    <?= $form->field($model, 'sku',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title',['options'=>['class'=>'col-lg-6 col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description',['options'=>['class'=>'col-sm-12 mt-3','style'=>'float: left;']])->textarea(['rows' => 6]) ?>

    <!-- <?//= $form->field($model, 'mainimage',['options'=>['class'=>'col-sm-12 mt-3','style'=>'float: left;']])->textInput(['maxlength' => true]) ?> -->
    <div class="row">
        <?= $form->field($model, 'imagen',['options'=>['class'=>'col-12 mt-3 bg-light','style'=>'float: left;']])->fileInput()->label('<div>Imagen: </div> <div class=" alert-warning" style="padding:4px; border-radius: 2px;"><small><i class="fas fa-info-circle"></i>&nbsp;&nbsp;Tamaño de Imagen: 100px X 100px (Ancho x Alto)</small></div>',['class'=>'col-12']) ?>
        <div id="preview" class="col-sm-12 col-md-6 mt-3" align="center"></div>
    </div>

    <?= $form->field($model, 'estatus',['options'=>['class'=>'col-sm-12 mt-3','style'=>'float: left;']])->dropDownList([ 'active' => 'Activo', 'inactive' => 'Inactivo', ], ['prompt' => 'Seleccione una opción']) ?>

</div>
<div class=" card-footer" align="right">
	<?=  Html::Button('<i class="fas fa-times-circle"></i> Cancelar', ['class' => 'btn btn-danger','id'=>'btnCloseForm','onClick'=>'closeForm("productForm")']) ?>
    <?= Html::submitButton('<i class="fas fa-check-circle"></i> Aceptar', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    document.getElementById("product-imagen").onchange = function(e) {
        if(e.target.files[0] === undefined){
            document.getElementById('preview').innerHTML = "";
        }else{
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(){
                let preview = document.getElementById('preview'),
                    image = document.createElement('img');

                image.src = reader.result;
                image.classList.add('img-fluid');

                preview.innerHTML = '';
                preview.append(image);
            };
        }
    }
JS;
$this->registerJs($script);

