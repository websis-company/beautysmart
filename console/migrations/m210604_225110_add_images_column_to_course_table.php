<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%course}}`.
 */
class m210604_225110_add_images_column_to_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'secondaryimage', $this->string());
        $this->addColumn('{{%course}}', 'thirdimage', $this->string());
        $this->addColumn('{{%course}}', 'bannerimage', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%course}}', 'secondaryimage');
        $this->dropColumn('{{%course}}', 'thirdimage');
        $this->dropColumn('{{%course}}', 'bannerimage');
    }
}
