<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%course}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%instructor}}`
 */
class m210617_041727_add_instructorid_column_to_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'instructor_id', $this->integer()->notNull());

        // creates index for column `instructor_id`
        $this->createIndex(
            '{{%idx-course-instructor_id}}',
            '{{%course}}',
            'instructor_id'
        );

        // add foreign key for table `{{%instructor}}`
        $this->addForeignKey(
            '{{%fk-course-instructor_id}}',
            '{{%course}}',
            'instructor_id',
            '{{%instructor}}',
            'instructor_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%instructor}}`
        $this->dropForeignKey(
            '{{%fk-course-instructor_id}}',
            '{{%course}}'
        );

        // drops index for column `instructor_id`
        $this->dropIndex(
            '{{%idx-course-instructor_id}}',
            '{{%course}}'
        );

        $this->dropColumn('{{%course}}', 'instructor_id');
    }
}
