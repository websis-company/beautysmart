<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m210708_035149_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'order_id'          => $this->primaryKey(),
            'profile_id'        => $this->integer()->notNull(),
            'payment_method'    => $this->string(20),
            'status'            => $this->string(20)->notNull(),
            'creation_date'     => $this->dateTime(),
            'modification_date' => $this->dateTime(),
            'payment_type'      => $this->string(45)
        ]);

        $this->addForeignKey(
            'fk-order-profile_id',
            'order',
            'profile_id',
            'profile',
            'profile_id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-profile_id',
            'order'
        );
        $this->dropTable('{{%order}}');
    }
}
