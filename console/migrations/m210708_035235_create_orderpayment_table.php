<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orderpayment}}`.
 */
class m210708_035235_create_orderpayment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orderpayment}}', [
            'orderpayment_id'    => $this->primaryKey(),
            'order_id'           => $this->integer()->notNull(),
            'code'               => $this->string(45),
            'orderTransactionId' => $this->string(150),
            'state'              => $this->string(60),
            'trazabilityCode'    => $this->string(150),
            'authorizationCode'  => $this->string(150),
            'responseCode'       => $this->string(150),
            'operationDate'      => $this->string(150),
            'updateDate'         => $this->string(150),
            'approvedDate'       => $this->string(150),
            'pendingreason'      => $this->string(200),
            'expirationdate'     => $this->string(60),
            'reference'          => $this->string(45),
            'barcode'            => $this->string(50),
            'urlpayment'         => $this->string(185),
            'totalpayment'       => $this->money(8,2)
        ]);

        $this->addForeignKey(
            'fk-orderpayment-order_id',
            'orderpayment',
            'order_id',
            'order',
            'order_id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-order_id',
            'orderpayment'
        );
        $this->dropTable('{{%orderpayment}}');
    }
}
