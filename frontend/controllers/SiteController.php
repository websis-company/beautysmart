<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\PaymentCourse;
use frontend\models\Profile;
use frontend\models\Order;
use frontend\models\Orderitem;
use frontend\models\Orderpayment;

//Backend
use backend\models\Category;
use backend\models\Course;
use backend\models\Instructor;

//Mercado Pago
use MercadoPago\SDK;
use MercadoPago\Payment;
use MercadoPago\Payer;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = Category::find()->where(['categoryparent_id' => null,'estatus' => 'active'])->all();
        return $this->render('index',['categories'=>$categories]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {

            if($model->saveContact()){
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            }else{
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }
            
            /*if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }*/

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    //Show popup Course detail
    public function actionCourse(){
        $idCourse = Yii::$app->request->get()["id"];
        $model    = Course::find()->where(["course_id" => $idCourse])->one();
        return $this->renderAjax('modalPopup', [
            'model' => $model,
        ]);
    }

    //Show popup Instructor Detail
    public function actionInstructor(){
        $idInstructor = Yii::$app->request->get()["id"];
        $model        = Instructor::find()->where(["instructor_id" => $idInstructor])->one();
        return $this->renderAjax('modalPopupInstructor', [
            'model' => $model,
        ]);
    }


    //Form to Pay Course
    public function actionInscription(){
        $idCourse = Yii::$app->request->get();
        if(isset($idCourse["id"])){
            $model        = Course::findOne(['course_id'=>$idCourse]);
            $modelPayment = new PaymentCourse();
            //$modelPayment->acceptconditions = 0;
            return $this->render('paymentcourse',[
                    'model'        => $model,
                    'modelPayment' => $modelPayment
                ]);
        }else{
            return $this->redirect(['index']);
        }
    }//end function

    //Create Payment Course (Mercado Pago)
    public function actionCreatepayment(){
        $paymentCourse   = Yii::$app->request->post()["PaymentCourse"];
        $mp_issuer       = Yii::$app->request->post()["issuer"];
        $mp_installments = Yii::$app->request->post()["installments"];
        $mp_token        = Yii::$app->request->post()["token"];
        $access_token    = Yii::$app->params["mercadopago"]["dev"]["seller"]["test_access_token"];

        /**
         * Save Client Info in Profile Table
         */
        $profileModel            = new Profile();
        $profileModel->firstname = $paymentCourse["namepayment"];
        $profileModel->lastname  = $paymentCourse["lastnamepayment"];
        $profileModel->email     = $paymentCourse["email"];
        $profileModel->phone     = $paymentCourse["phonepayment"];

        if($profileModel->validate()){
            //save
            $profileModel->save();
        }else{
            //return error
            echo "<pre>";
            var_dump($profileModel->errors);
            echo "</pre>";
            die();
        }//end if

        /**
         * Save Order
         */
        $orderModel                 = new Order();
        $orderModel->profile_id     = $profileModel->profile_id;
        $orderModel->payment_method = "on_line";
        $orderModel->status         = "pending";
        $orderModel->creation_date  = date("Y-m-d H:i:s");
        $orderModel->payment_type   = "";
        if($orderModel->validate()){
            //save
            $orderModel->save();
        }else{
            //return error
            echo "<pre>";
            var_dump($orderModel->errors);
            echo "</pre>";
        }

        /**
         * Save Order
         */
        $modelCourse = Course::find()->where(["course_id" => $paymentCourse["courseId"]])->one();
        $coursename  = $modelCourse->title == 'basico' ? 'Básico' : 'Avanzado';

        $orderItemModel              = new Orderitem();
        $orderItemModel->order_id    = $orderModel->order_id;
        $orderItemModel->product_id  = $modelCourse->course_id;
        $orderItemModel->typeproduct = "course";
        $orderItemModel->sku         = $modelCourse->code;
        $orderItemModel->name        = $modelCourse->category->name." - ".$coursename;
        $orderItemModel->price       = $modelCourse->price;
        $orderItemModel->quantity    = 1;

        if($orderItemModel->validate()){
            $orderItemModel->save();
        }else{
            //return error
            echo "<pre>";
            var_dump($orderItemModel->errors);
            echo "</pre>";
        }//end if


        /**
         * Create a payment referene (save in server side)
         * @var external_reference for indentification paymente in server side
         */
        $external_reference = $profileModel->profile_id."|".$orderModel->order_id."|".$orderItemModel->sku;

        //Mercado Pago SDK
        $mp           = new SDK();
        $mp->initialize();
        $config = $mp->config();
        $config->set('ACCESS_TOKEN', $access_token);
        //$mp::setAccessToken($access_token);

        //Mercado Pago Payment Information
        $payment                     = new Payment();
        $payment->transaction_amount = (float) $paymentCourse["transactionAmount"];
        $payment->token              = $mp_token;
        $payment->description        = $paymentCourse["description"];
        $payment->installments       = (int) $mp_installments;
        $payment->payment_method_id  = $paymentCourse["paymentMethodId"];
        $payment->issuer_id          = (int) $mp_issuer;
        $payment->external_reference = $external_reference;

        //Mercado Pago Payer Information
        $payer             = new Payer();
        $payer->email      = $paymentCourse["email"];
        $payer->first_name = $paymentCourse["namepayment"];
        $payer->last_name  = $paymentCourse["lastnamepayment"];

        $payment->payer = $payer;

        try {
            $payment->save();
        } catch (Exception $e) {
            echo "<h1>Error</h1>";
            echo "<pre>";
            var_dump($e);
            echo "</pre>";
            die();
        }

        //$payment->save();
        echo "<pre>";
        print_r($payment);
        echo "</pre>";
        die();

        $response = array(
            'status' => $payment->status,
            'status_detail' => $payment->status_detail,
            'id' => $payment->id
        );
        echo json_encode($response);


        echo "<pre>";
        var_dump($payment);
        echo "</pre>";

        echo "<pre>";
        var_dump($payer);
        echo "</pre>";
        die();


        /*echo "<pre>";
        print_r($init);
        echo "</pre>";

        echo "<pre>";
        print_r($config);
        echo "</pre>";*/

        echo "<pre>";
        print_r($access_token);
        echo "</pre>";

        echo "<pre>";
        var_dump($paymentCourse);
        echo "</pre>";

        echo "<pre>";
        var_dump($mp_issuer);
        echo "</pre>";

        echo "<pre>";
        var_dump($mp_installments);
        echo "</pre>";

        echo "<pre>";
        var_dump($mp_token);
        echo "</pre>";
        die();

    }//end function


    /**** Create User Test For Mercado Pago Payments ****/
    /**
     * @view globalassets/mercadogpago.txt
     */
    /*public function actionCreatetestusermp(){
        $prod_credentials_mp = Yii::$app->params["mercadopago"]["prod"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.mercadopago.com/users/test_user');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"site_id\":\"MLM\"}");
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.$prod_credentials_mp["test_access_token"];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }//end if
        curl_close($ch);

        $response_array = json_decode($result,true);
        echo "<pre>";
        var_dump($response_array);
        echo "</pre>";
        die();
    }*/


    

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {  
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
