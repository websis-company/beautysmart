<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property int $order_id
 * @property int $profile_id
 * @property string|null $payment_method
 * @property string $status
 * @property string|null $creation_date
 * @property string|null $modification_date
 * @property string|null $payment_type
 *
 * @property Profile $profile
 * @property Orderitem[] $orderitems
 * @property Orderpayment[] $orderpayments
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'status'], 'required'],
            [['profile_id'], 'integer'],
            [['creation_date', 'modification_date'], 'safe'],
            [['payment_method', 'status'], 'string', 'max' => 20],
            [['payment_type'], 'string', 'max' => 45],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'profile_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'profile_id' => 'Profile ID',
            'payment_method' => 'Payment Method',
            'status' => 'Status',
            'creation_date' => 'Creation Date',
            'modification_date' => 'Modification Date',
            'payment_type' => 'Payment Type',
        ];
    }

    /**
     * Gets query for [[Profile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['profile_id' => 'profile_id']);
    }

    /**
     * Gets query for [[Orderitems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderitems()
    {
        return $this->hasMany(Orderitem::className(), ['order_id' => 'order_id']);
    }

    /**
     * Gets query for [[Orderpayments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderpayments()
    {
        return $this->hasMany(Orderpayment::className(), ['order_id' => 'order_id']);
    }
}
