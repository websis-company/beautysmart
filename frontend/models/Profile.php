<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property int $profile_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property int|null $phone
 *
 * @property Order[] $orders
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * {@inheritdoc}
     */
    /*public function rules()
    {
        return [
            [['firstname', 'lastname', 'email'], 'required'],
            [['phone'], 'integer'],
            [['firstname', 'lastname', 'email'], 'string', 'max' => 255],
        ];
    }*/

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profile_id' => 'Profile ID',
            'firstname'  => 'Nombre(s)',
            'lastname'   => 'Apellidos',
            'email'      => 'Correo Eléctronico',
            'phone'      => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['profile_id' => 'profile_id']);
    }
}
