<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\FontAsset;
use common\widgets\Alert;

AppAsset::register($this);
FontAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo.svg',['alt'=>'Beauty Smart','class'=>'img-fluid','style'=>'width: 80px; height: auto;']),
        'brandOptions' => ['class'=>'mr-0'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'sticky-top navbar-expand-lg navbar-light bg-white',
        ],
    ]);
    $menuItems = [
        ['label' => 'INICIO','url' => ['/site/index'],'options'=>['class'=>'px-5']],
        ['label' => 'NOSOTROS', 'url' => ['/site/aboutus'],'options'=>['class'=>'px-5']],
        ['label' => 'CONTACTO', 'url' => ['/site/contact'],'options'=>['class'=>'px-5']],
        ['label' => 'SERVICIOS', 'url' => ['/site/service'],'options'=>['class'=>'px-5']]
    ];
    /*if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }*/
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav mr-auto'],
        'items' => $menuItems,
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => Html::img('@web/images/store.svg',['alt'=>'Store','width'=>'25px','height'=>'25px']),'url' => ['/site/signup'],'options'=>['class'=>'btnStore px-2','title'=>'Registrarse']],
            ['label' => Html::img('@web/images/login.svg',['alt'=>'Login','width'=>'25px','height'=>'25px']),'url' => ['/site/login'],'options'=>['class'=>'btnLogin px-2','title'=>'Iniciar Sesión']],
        ],
        'encodeLabels' => false,
    ]);
    NavBar::end();

    ?>

    
    <div class="toolbar">
        <div class="toolbar-item">
            <?php echo Html::a(Html::img('@web/images/ig-icon.svg',['style'=>'width: 35px; height: 35px;']), $url = Yii::$app->params["social-networks"]["instagram"],['title'=>'Instagram','target'=>'_blank']) ?>
        </div>
        <div class="toolbar-item">
            <?php echo Html::a(Html::img('@web/images/wa-icon.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->params["social-networks"]["whatsapp"],['title'=>'whatsapp','target'=>'_blank']) ?>
        </div>
        <div class="toolbar-item">
            <?php echo Html::a(Html::img('@web/images/fb-icon.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->params["social-networks"]["facebook"],['title'=>'facebook','target'=>'_blank']) ?>
        </div>
    </div>

    <div class="toolbar-dark">
        <div class="toolbar-item-dark">
            <?php echo Html::a(Html::img('@web/images/ig-icon-dark.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->params["social-networks"]["instagram"],['title'=>'Instagram','target'=>'_blank']) ?>
        </div>
        <div class="toolbar-item-dark">
            <?php echo Html::a(Html::img('@web/images/wa-icon-dark.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->params["social-networks"]["whatsapp"],['title'=>'whatsapp','target'=>'_blank']) ?>
        </div>
        <div class="toolbar-item-dark">
            <?php echo Html::a(Html::img('@web/images/fb-icon-dark.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->params["social-networks"]["facebook"],['title'=>'facebook','target'=>'_blank']) ?>
        </div>
    </div>    

    <!-- <div class="container"> -->
        <?/*= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>
        <?= Alert::widget() ?>
        <?= $content ?>
    <!-- </div> -->
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-4 footer-sitemap">
                <p><?= Html::a("INICIO", $url = Url::to(['/']), ['option' => 'value']); ?></p>
                <p><?= Html::a("NOSOTROS", $url = Url::to(['/']), ['option' => 'value']); ?></p>
                <p><?= Html::a("CONTACTO", $url = Url::to(['/']), ['option' => 'value']); ?></p>
                <p><?= Html::a("SERVICIOS", $url = Url::to(['/']), ['option' => 'value']); ?></p>
            </div>
            <div class="col-6 col-md-4 footer-social-media">
                <p>Síguenos en </p>
                <p class="social-media">
                    <?php echo Html::a(Html::img('@web/images/ig-icon.svg',['style'=>'width: 35px; height: 35px;']), $url = Yii::$app->homeUrl,['title'=>'Instagram']) ?>
                    <?php echo Html::a(Html::img('@web/images/wa-icon.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->homeUrl,['title'=>'whatsapp']) ?>
                    <?php echo Html::a(Html::img('@web/images/fb-icon.svg',['style'=>'width: 35px; height: 35px;']),$url = Yii::$app->homeUrl,['title'=>'facebook']) ?>
                </p>

                <p class="footer-email">
                    Escríbenos
                    <br>
                    <?= Html::a('hola@beautysmart.com', $url = null, ['class' => '']); ?>
                </p>
            </div>
            <div class="col-12 col-md-4 footer-location">
                <p><?=Html::img('@web/images/ubicacion_icon.svg',['style'=>'width: 25px; height: 25px; margin-top: -10px'])?> Puebla, Puebla</p>
                <p class="footer-email"><?= Html::a("Términos y Condiciones", $url = Url::to(['/']), ['option' => 'value']); ?></p>
            </div>
            <div class="col-12 footer-legals">
                <span>© <?= date('Y') ?> Beauty Smart. Todos los derechos reservados.</span>        
            </div>
        </div>
        <!-- <p class="float-left">&copy; <?//= Html::encode(Yii::$app->name) ?> <?//= date('Y') ?></p>
        <p class="float-right"><?//= Yii::powered() ?></p> -->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
