<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid site-login-wrap ">
    <main role="main" class="container site-login-container d-flex align-items-center">
        <div class="d-flex justify-content-center">
            <div class="col-12 col-lg-5">
                <!-- <h1><?//= Html::encode($this->title) ?></h1>
                <p>Please fill out the following fields to login:</p> -->

                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                            <?= $form->field($model, 'username')->textInput(['placeholder'=>'Nombre de usuario'])->label(false) ?>

                            <?= $form->field($model, 'password',['options'=>['class'=>'mt-5']])->passwordInput(['placeholder'=>'Contraseña'])->label(false) ?>

                            <?//= $form->field($model, 'rememberMe')->checkbox() ?>

                            <!-- <div style="color:#999;margin:1em 0">
                                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                <br>
                                Need new verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
                            </div> -->

                            <div class="form-group text-center">
                                <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-light mt-5', 'name' => 'login-button']) ?>
                            </div>

                            <div class="pt-5">
                                <div class="text-center pb-3">
                                    O ¿Has olvidado tu contraseña?
                                </div>
                                <div class="text-center pt-2">
                                    ¿No tienes una cuenta? <strong>Regístrate</strong>
                                </div>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>