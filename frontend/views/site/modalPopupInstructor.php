<?php 
//use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

$ins_name       = $model->firstname." ".$model->lastname;
$ins_trajectory = $model->trajectory;
$ins_perfilimg  = $model->perfilimage;
$ins_fb         = $model->fblink;
$ins_ig         = $model->iglink;
$ins_wp         = $model->wplink;

$ins_image1     = $model->image1;
$ins_image2     = $model->image2;
$ins_image3     = $model->image3;
$ins_image4     = $model->image4;

$show_gallery = false;
if(!empty($ins_image1) && !empty($ins_image2) && !empty($ins_image3) && !empty($ins_image4)){
    $show_gallery = true;
}//end if

?>

<!-- Modals PopUp -->
<div id="modal-instructor" class="col-xl-7 col-lg-8 col-md-9 col-sm-9">
    <div class="container-fluid">
        <div class="row">
            <div class="<?php echo $show_gallery == true ? "col-md-6" : "" ?> col-12">
                <div class="row modal-instructor-head">
                    <div class="col-4">
                        <?= Html::img(Url::base().'/backend/web/'.$ins_perfilimg, ['class' => 'img-fluid']); ?>
                    </div>
                    <div class="col-8">
                        <div class="modal-instructor-title">Instructor</div>
                        <div class="modal-instructor-subtitle"><?php echo $ins_name; ?></div>
                    </div>
                </div>
                <div class="row modal-instructor-detail">
                    <div class="col-12">
                        <div class="modal-instructor-detail-title">
                            Trayectoria
                        </div>
                        <div class="modal-instructor-detail-text">
                            <?php echo nl2br($ins_trajectory) ?>
                        </div>
                    </div>
                </div>
                <div class="row modal-instructor-share">
                    <div class="col-12">
                        <div class="modal-instructor-share-title">
                            Redes Sociales
                        </div>
                    </div>
                    <div class="row modal-instructor-share-icons">
                        <div class="col-4">
                            <?php 
                            if(!empty($ins_ig))
                                echo Html::a(Html::img('@web/images/ig-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']), $url = $ins_ig ,['title'=>'Instagram','target'=>'_blank']) 
                            ?>
                        </div>
                        <div class="col-4">
                            <?php 
                            if(!empty($ins_wp))
                                echo Html::a(Html::img('@web/images/wa-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']),$url = "https://wa.me/52".$ins_wp, ['title'=>'whatsapp','target'=>'_blank']) 
                            ?>
                        </div>
                        <div class="col-4">
                            <?php 
                            if(!empty($ins_fb))
                                echo Html::a(Html::img('@web/images/fb-icon.svg',['class'=>'img-fluid','style'=>'width: 35px; height: auto;']),$url = $ins_fb,['title'=>'facebook','target'=>'_blank']) 
                            ?>
                        </div>
                    </div>
                </div>                
            </div>

            <?php 
            if ($show_gallery == true) {
            ?>
            <div class="col-md-6 col-12">
                <div class="row">
                    <div class="col-6">
                        <?= Html::img(Url::base().'/backend/web/'.$ins_image1, ['class' => 'img-fluid modal-instructor-gallery']); ?>
                        <?= Html::img(Url::base().'/backend/web/'.$ins_image4, ['class' => 'img-fluid modal-instructor-gallery']); ?>
                    </div>
                    <div class="col-6">
                        <?= Html::img(Url::base().'/backend/web/'.$ins_image2, ['class' => 'img-fluid modal-instructor-gallery']); ?>
                        <?= Html::img(Url::base().'/backend/web/'.$ins_image3, ['class' => 'img-fluid modal-instructor-gallery']); ?>
                    </div>
                </div>
            </div>
            <?php 
            }//end if
            ?>
        </div>
    </div>
</div>
<!-- End Modals PopUp -->