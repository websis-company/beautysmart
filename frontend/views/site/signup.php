<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid site-signup-wrap">
    <main role="main" class="container site-signup-container d-flex align-items-center">
        <div class="d-flex justify-content-center">
            <div class="col-12 col-lg-5">
                <!-- <h1><?= Html::encode($this->title) ?></h1>

                <p>Please fill out the following fields to signup:</p> -->

                <div class="row">
                    <div class="col-12">
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                            <?= $form->field($model, 'username')->textInput(['placeholder'=>'Nombre Completo'])->label(false) ?>

                            <?= $form->field($model, 'email',['options'=>['class'=>'mt-5']])->textInput(['placeholder'=>'Correo Electrónico'])->label(false) ?>

                            <?= $form->field($model, 'password',['options'=>['class'=>'mt-5']])->passwordInput(['placeholder'=>'Contraseña'])->label(false) ?>

                            <div class="form-group text-center">
                                <?= Html::submitButton('Regístrate', ['class' => 'btn btn-light mt-5', 'name' => 'signup-button']) ?>
                            </div>

                            <div class="pt-5">
                                <div class="text-center pb-3">
                                    Al registrate, aceptas nuestras <strong>Condiciones de uso y Política de privacidad</strong>
                                </div>
                                <div class="text-center pt-2">
                                    ¿Ya tienes cuenta? <strong>Iniciar Sesión</strong>
                                </div>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
