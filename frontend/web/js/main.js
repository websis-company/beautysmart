/*** Btn Login ***/
$(".btnLogin").on("mouseover", function(){
	$(this).find('img').attr('src','/beautysmart/images/login-red.svg');
});
$(".btnLogin").on("mouseout", function(){
	$(this).find('img').attr('src','/beautysmart/images/login.svg');
});

/*** Btn Store ***/
$(".btnStore").on("mouseover", function(){
	$(this).find('img').attr('src','/beautysmart/images/store-red.svg');
});
$(".btnStore").on("mouseout", function(){
	$(this).find('img').attr('src','/beautysmart/images/store.svg');
});

/*** Toolbar ***/
$(window).scroll(function(){
	var scroll = $(window).scrollTop();
	var windows = $( window ).width();

	if(scroll >= 600 && windows > 1300){
		$(".toolbar-dark").show();
		$(".toolbar").hide();
	}else if(scroll < 600 && windows > 1300){
		$(".toolbar-dark").hide();
		$(".toolbar").show();
	}//end if

	/*** stand by ***/
	/*if(scroll >= 279 && windows < 1300){
		$(".toolbar-dark").css("background","none");
	}else if(scroll < 279 && windows < 1300){
		$(".toolbar-dark").css("background","white");
	}*/
});